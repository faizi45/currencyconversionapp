package com.sample.faizan.currencyconversionapp.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.sample.faizan.currencyconversionapp.R
import com.sample.faizan.currencyconversionapp.databinding.ActivityHomeBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class HomeActivity : AppCompatActivity() {

    private var activityHomeBinding: ActivityHomeBinding? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activityHomeBinding = ActivityHomeBinding.inflate(layoutInflater)

        setContentView(activityHomeBinding?.root)

        addFragmentToActivity(HomeFragment())
    }

    private fun addFragmentToActivity(fragment: Fragment?) {

        if (fragment == null) return
        val fm = supportFragmentManager
        val tr = fm.beginTransaction()
        tr.add(R.id.fragmentContainerView, fragment)
        tr.commitAllowingStateLoss()
    }
}