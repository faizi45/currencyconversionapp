package com.sample.faizan.currencyconversionapp.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.sample.faizan.currencyconversionapp.R
import com.sample.faizan.currencyconversionapp.models.ExchangeRateEntity

class ConvertedRatesAdapter(private val callback: (ExchangeRateEntity) -> Unit) :
    RecyclerView.Adapter<ConvertedRatesAdapter.ConvertedRatesViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ConvertedRatesViewHolder {

        return ConvertedRatesViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_currency, parent, false)
        )
    }

    override fun onBindViewHolder(holder: ConvertedRatesViewHolder, position: Int) {
        val currentItem = differ.currentList[position]

        holder.tvCurrency.text = ""
        holder.tvCurrency.text = currentItem.currency + " : "
        holder.tvRate.text = currentItem.amount.toString()

        holder.itemView.setOnClickListener {
            callback.invoke(currentItem)
        }
    }

    override fun getItemCount(): Int {
        return differ.currentList.size
    }

    inner class ConvertedRatesViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val tvCurrency = itemView.findViewById<TextView>(R.id.tvCurrencyName)
        val tvRate = itemView.findViewById<TextView>(R.id.tvRate)

    }


    private val differCallBack = object : DiffUtil.ItemCallback<ExchangeRateEntity>() {
        override fun areItemsTheSame(
            oldItem: ExchangeRateEntity,
            newItem: ExchangeRateEntity
        ): Boolean {
            return oldItem.sourceCurrency == newItem.sourceCurrency
        }

        override fun areContentsTheSame(
            oldItem: ExchangeRateEntity,
            newItem: ExchangeRateEntity
        ): Boolean {
            return oldItem == newItem
        }
    }

    val differ = AsyncListDiffer(this, differCallBack)
}