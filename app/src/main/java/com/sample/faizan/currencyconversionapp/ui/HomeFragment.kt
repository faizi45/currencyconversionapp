package com.sample.faizan.currencyconversionapp.ui

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import com.sample.faizan.currencyconversionapp.R
import com.sample.faizan.currencyconversionapp.adapters.ConvertedRatesAdapter
import com.sample.faizan.currencyconversionapp.adapters.CurrenciesSpinnerAdapter
import com.sample.faizan.currencyconversionapp.models.ExchangeRateEntity
import com.sample.faizan.currencyconversionapp.models.Status
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_home.*

@AndroidEntryPoint
class HomeFragment : Fragment() {

    private var customAdapter: CurrenciesSpinnerAdapter? = null
    private val TAG = HomeFragment::class.java.simpleName

    private val viewModel: HomeViewModel by viewModels()

    private var conversionRatesList: HashMap<String, Double>? = null
    private var conversionRatesListExchangeEntity: List<ExchangeRateEntity>? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initRecyclerView()
        listeners()

        observeDataNBR()
//        observeData()
    }

    fun observeDataNBR() {

        viewModel.currenciesList?.observe(viewLifecycleOwner, Observer { baseResponse ->

            Log.d(TAG, "observeDataNBR: ${baseResponse.status}")
            Log.d(TAG, "observeDataNBR: ${baseResponse.data?.size}")

            when (baseResponse.status) {

                Status.LOADING -> {

                }

                Status.SUCCESS -> {

                    customAdapter =
                        baseResponse.data?.let {
                            CurrenciesSpinnerAdapter(requireContext(), it)
                        }

                    spinner?.adapter = customAdapter

                    listeners()

                }

                Status.ERROR -> {

                    customAdapter =
                        baseResponse.data?.let {
                            CurrenciesSpinnerAdapter(requireContext(), it)
                        }
                    spinner?.adapter = customAdapter

                    listeners()
                }
            }
        })


        viewModel.exchangeRatesList?.observe(viewLifecycleOwner, Observer { baseResponse ->

            Log.d(TAG, "observeDataNBR:exchangeRatesListNBR ${baseResponse.status}")
            Log.d(TAG, "observeDataNBR:exchangeRatesListNBR ${baseResponse.data?.size}")

            when (baseResponse.status) {

                Status.LOADING -> {
                    Log.d(TAG, "observeDataNBR: Status.LOADING")

                }

                Status.SUCCESS -> {
                    Log.d(TAG, "observeDataNBR: Status.SUCCESS")

                    conversionRatesListExchangeEntity = baseResponse.data
                }

                Status.ERROR -> {

                    Log.d(TAG, "observeDataNBR: Status.ERROR")

                    conversionRatesListExchangeEntity = baseResponse.data

                }
            }
        })
    }

    fun initRecyclerView() {
        rvAllCurrencies?.layoutManager = androidx.recyclerview.widget.LinearLayoutManager(
            activity,
            androidx.recyclerview.widget.LinearLayoutManager.VERTICAL, false
        )

    }

    private fun listeners() {
        //todo handle spinner dropdown click
        //in onclick of that when a value of FROM/source currency is chosen lets say AED
        //we need to use the formula to handle the conversion
        spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {

                var currencyListEntity = customAdapter?.getItem(position)
                var fromCurrencyKey = currencyListEntity?.currency

                Log.d(TAG, "onItemSelected: key $fromCurrencyKey")
                var amountEntered = etCurrencyValue?.text?.toString()

                if (amountEntered.isNullOrEmpty())
                    etCurrencyValue.setError("Enter correct Amount")
                else
                    convert(fromCurrencyKey, amountEntered)
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
            }
        }

    }

    private fun convert(
        fromCurrency: String?,
        amountEntered: String?
    ) {

        var resultList = mutableListOf<ExchangeRateEntity>()

        var fromCurrencyRateInUsd: ExchangeRateEntity? = null
        //first find the value of currency in usd
        fromCurrencyRateInUsd = conversionRatesListExchangeEntity?.find {
            it.currency.contains(fromCurrency.toString())
        }

        var one: Double = 1.0
        val div = fromCurrencyRateInUsd?.let {
            one.div(it.amount)
        } // 1/fromCurrencyinUsd

        //(1/fromCurrencyInUSD)* amountEnterered
        var r1 = amountEntered?.toDouble()?.let { amount ->
            (div)?.times(amount)
        }

        conversionRatesListExchangeEntity?.let { list ->
            //iterate over all the USD->XYZ currencies in the list and multiply our r1 amount with that amount to get final result
            list.forEach {
                var result = r1?.times(it.amount)

                Log.d(TAG, "convert: userSelectedCurrency $fromCurrency")
                Log.d(TAG, "convert: it.source ${it.sourceCurrency}")

                var sourceCurrency = "USD"
                if (it.sourceCurrency.isEmpty()) {
                    sourceCurrency = "USD"
                } else
                    sourceCurrency = it.sourceCurrency

                Log.d(TAG, "convert: userSelectedCurrency $fromCurrency")
                Log.d(TAG, "convert: it.source ${sourceCurrency}")
                it.currency.substringAfter(fromCurrency.toString())


                var exchangeRateEntity = ExchangeRateEntity(
                    sourceCurrency = fromCurrency.toString(),
                    currency = fromCurrency + it.currency.takeLast(3),
                    amount = result!!
                )
                resultList.add(exchangeRateEntity)

            }

            var adapter = ConvertedRatesAdapter() {

            }
            rvAllCurrencies.adapter = adapter

            adapter.differ.submitList(resultList)
            conversionRatesListExchangeEntity = resultList
        }

    }

}