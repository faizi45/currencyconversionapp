package com.sample.faizan.currencyconversionapp.models

import com.google.gson.annotations.SerializedName

data class LiveConversionResponseDto(

    @SerializedName("success") var success: Boolean? = null,
    @SerializedName("terms") var terms: String? = null,
    @SerializedName("privacy") var privacy: String? = null,
    @SerializedName("timestamp") var timestamp: Int? = null,

    @SerializedName("source") var source: String? = null,

    @SerializedName("quotes")
    var conversionRate: HashMap<String, Double> = HashMap()

)