package com.sample.faizan.currencyconversionapp.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import com.sample.faizan.currencyconversionapp.R
import com.sample.faizan.currencyconversionapp.models.CurrencyListEntity

class CurrenciesSpinnerAdapter(context: Context, var list: List<CurrencyListEntity>) :
    BaseAdapter() {

    val mInflater: LayoutInflater = LayoutInflater.from(context)

    override fun getCount(): Int {
        return list.size
    }

    override fun getItem(position: Int): CurrencyListEntity {
        return list[position]
    }

    override fun getItemId(position: Int): Long {
        return list[position].id?.toLong() ?: 0

    }


    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val view: View = mInflater.inflate(R.layout.spinner_custom_layout, parent, false)

        view.findViewById<TextView>(R.id.tvSpinnerText).text = list[position].currency

        return view
    }


}