package com.sample.faizan.currencyconversionapp.repository

import android.util.Log
import androidx.lifecycle.LiveData
import com.sample.faizan.currencyconversionapp.BuildConfig
import com.sample.faizan.currencyconversionapp.api.ApiService
import com.sample.faizan.currencyconversionapp.api.AppExecutors
import com.sample.faizan.currencyconversionapp.api.NetworkBoundResource
import com.sample.faizan.currencyconversionapp.database.CurrenciesListDao
import com.sample.faizan.currencyconversionapp.models.*
import javax.inject.Inject

class CurrencyRepository @Inject constructor(
    val apiService: ApiService, val dao: CurrenciesListDao,
    private val appExecutors: AppExecutors
) {

    fun getCurrenciesList(): LiveData<BaseResponse<List<CurrencyListEntity>>> {

        return object :
            NetworkBoundResource<List<CurrencyListEntity>, BaseResponse<CurrenciesListResponseDto>>(
                appExecutors
            ) {
            var currencies: HashMap<String, String> = HashMap()

            override fun saveCallResult(item: BaseResponse<CurrenciesListResponseDto>) {
                //check if data is not null and success etc
                var list = mutableListOf<CurrencyListEntity>()
                if (item.data?.success == true) {
                    //save to db
                    currencies = item.data?.currencies ?: HashMap()

                    item.data?.currencies?.let { it ->
                        it.forEach { (key, valu) ->

                            list.add(
                                CurrencyListEntity(
                                    currency = key,
                                    currencyLabel = valu,
                                    lastSyncAt = System.currentTimeMillis()
                                )
                            )

                        }
                    }
                    dao.deleteAllCurrenciesList()
                    dao.upsertCurrenciesList(list)
                }
            }

            override fun shouldFetch(data: List<CurrencyListEntity>?): Boolean {
                //check of 30 mints as per req
                if (data != null && data.isNotEmpty()) { //becz first time data list is of 0 size

                    var lastSyncAt = data.get(0).lastSyncAt

                    var timeDiff = System.currentTimeMillis() - lastSyncAt
                    var mints = (timeDiff.div(1000)).div(60)

                    Log.d("CurrencyRepository", "shouldFetch: Mints $mints")

                    return mints > 30

                } else
                    return true

            }

            override fun loadFromDb(): LiveData<List<CurrencyListEntity>> {
                //load from db using dao

                return dao.getCurrenciesList()
            }

            override fun createCall(): LiveData<BaseResponse<CurrenciesListResponseDto>> {
                return apiService.getCurrenciesList(BuildConfig.API_KEY)

            }

            override fun onFetchFailed() {
                loadFromDb()
            }
        }.asLiveData()
    }

    fun getExchangeRates(): LiveData<BaseResponse<List<ExchangeRateEntity>>> {

        return object :
            NetworkBoundResource<List<ExchangeRateEntity>, BaseResponse<LiveConversionResponseDto>>(
                appExecutors
            ) {
            var rates: HashMap<String, Double> = HashMap()

            override fun saveCallResult(item: BaseResponse<LiveConversionResponseDto>) {
                //check if data is not null and success etc
                var list = mutableListOf<ExchangeRateEntity>()
                if (item.data?.success == true) {
                    //save to db
                    rates = item.data?.conversionRate ?: HashMap()
                    item.data?.conversionRate?.let { it ->
                        it.forEach { (key, valu) ->

                            list.add(
                                ExchangeRateEntity(
                                    sourceCurrency = "USD",
                                    currency = key,
                                    amount = valu
                                )
                            )

                        }
                    }
                    dao.deleteAllExchangeRates()
                    dao.upsertExchangeRatesList(list)
                }
            }

            override fun shouldFetch(data: List<ExchangeRateEntity>?): Boolean {
                //check of 30 mints as per req
                if (data != null && data.isNotEmpty()) { //becz first time data list is of 0 size

                    var lastSyncAt = data.get(0).lastSyncAt

                    var timeDiff = System.currentTimeMillis() - lastSyncAt
                    var mints = (timeDiff.div(1000)).div(60)
                    Log.d("CurrencyRepository", "shouldFetch: Mints $mints")

                    return mints > 30

                } else
                    return true

            }

            override fun loadFromDb(): LiveData<List<ExchangeRateEntity>> {
                //load from db using dao
                return dao.getExchangeRatesList()
            }

            override fun createCall(): LiveData<BaseResponse<LiveConversionResponseDto>> {
                return apiService.getLiveExchangeRates(BuildConfig.API_KEY)
            }

        }.asLiveData()
    }

}