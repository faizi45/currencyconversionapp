package com.sample.faizan.currencyconversionapp.database

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.sample.faizan.currencyconversionapp.models.CurrencyListEntity
import com.sample.faizan.currencyconversionapp.models.ExchangeRateEntity

@Dao
interface CurrenciesListDao {

    @Query("SELECT * FROM currency_entity ORDER BY currencyLabel ASC")
    fun getCurrenciesList(): LiveData<List<CurrencyListEntity>>

    @Query("DELETE FROM currency_entity")
    fun deleteAllCurrenciesList()

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun upsertCurrenciesList(list: List<CurrencyListEntity>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun upsertExchangeRatesList(list: List<ExchangeRateEntity>)

    @Query("SELECT * FROM exchange_rate_entity ORDER BY currency ASC")
    fun getExchangeRatesList(): LiveData<List<ExchangeRateEntity>>

    @Query("DELETE FROM exchange_rate_entity")
    fun deleteAllExchangeRates()


}