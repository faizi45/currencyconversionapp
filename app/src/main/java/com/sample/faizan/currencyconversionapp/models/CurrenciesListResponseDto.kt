package com.sample.faizan.currencyconversionapp.models

import com.google.gson.annotations.SerializedName


data class CurrenciesListResponseDto(

    @SerializedName("success") var success: Boolean? = null,
    @SerializedName("terms") var terms: String? = null,
    @SerializedName("privacy") var privacy: String? = null,

    @SerializedName("currencies") var currencies: HashMap<String, String> = HashMap()

)