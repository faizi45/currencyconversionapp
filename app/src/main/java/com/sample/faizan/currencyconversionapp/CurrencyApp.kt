package com.sample.faizan.currencyconversionapp

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class CurrencyApp : Application() {
}