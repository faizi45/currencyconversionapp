package com.sample.faizan.currencyconversionapp.api

import android.util.Log
import androidx.annotation.MainThread
import androidx.annotation.WorkerThread
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import com.sample.faizan.currencyconversionapp.models.BaseResponse
import com.sample.faizan.currencyconversionapp.models.Status

/**
 * A generic class that can provide a resource backed by both the sqlite database and the network.
 *
 *
 * You can read more about it in the [Architecture
 * Guide](https://developer.android.com/arch).
 * @param <ResultType>
 * @param <RequestType>
</RequestType></ResultType> */

abstract class NetworkBoundResource<ResultType, RequestType>
@MainThread constructor(
    val appExecutorsJv: AppExecutors
) {

    private val TAG: String = NetworkBoundResource::class.java.simpleName.toString()

    private val result = MediatorLiveData<BaseResponse<ResultType>>()

    init {
        Log.d(TAG, "init{}:")

        result.value = BaseResponse.loading(null)
        @Suppress("LeakingThis")
        val dbSource = loadFromDb()

        result.addSource(dbSource) { data ->
            Log.d(TAG, "addSource():")
            Log.d(TAG, "_:" + dbSource.value.toString())

            result.removeSource(dbSource)
            if (shouldFetch(data)) {
                Log.d(TAG, "shouldFetch(): True")

                fetchFromNetwork(dbSource)
            } else {
                Log.d(TAG, "shouldFetch(): False")

                result.addSource(dbSource) { newData ->
                    setValue(BaseResponse.success(newData))
                }
            }

        }
    }

    @MainThread
    private fun setValue(newValue: BaseResponse<ResultType>) {
        Log.d(TAG, "setValue():")
//        Log.d(TAG, "new Value:- ${newValue.toString()}")

        if (result.value != newValue) {

            Log.d(TAG, "new Value:- ${newValue.data}")
            result.value = newValue
        }
    }


    private fun fetchFromNetwork(dbSource: LiveData<ResultType>) {

        Log.d(TAG, "fetchFromNetwork(): ")
        val apiResponse = createCall()
        // we re-attach dbSource as a new source, it will dispatch its latest value quickly
        result.addSource(dbSource) { newData ->
            setValue(BaseResponse.loading(newData))
        }
        result.addSource(apiResponse) { response ->
            result.removeSource(apiResponse)
            result.removeSource(dbSource)
            Log.d(TAG, "result.addSource: ${(response as BaseResponse<*>).status}")

            when ((response as BaseResponse<*>).status) {
                Status.SUCCESS -> {
                    Log.d(TAG, "Status.SUCCESS ")

                    appExecutorsJv.diskIO().execute {
                        processResponse((response))?.let { requestType ->
                            requestType?.let {
                                saveCallResult(it)
                            }
                        }

                        appExecutorsJv.mainThread().execute {
                            // we specially request a new live data,
                            // otherwise we will get immediately last cached value,
                            // which may not be updated with latest results received from network.
                            loadFromDb().let {
                                result.addSource(it) { newData ->
                                    setValue(BaseResponse.success(newData))
                                }
                            }
                        }
                    }
                }
                Status.LOADING -> {
                    Log.d(TAG, "Status.LOADING ")


                    appExecutorsJv.mainThread().execute {
                        // reload from disk whatever we had
                        loadFromDb().let { dbData ->
                            result.addSource(dbData) { newData ->
                                setValue(BaseResponse.success(newData))
                            }
                        }
                    }
                }

                Status.ERROR -> {
                    Log.d(TAG, "Status.ERROR ")

                    onFetchFailed()
                    result.addSource(dbSource) { newData ->
                        setValue(BaseResponse.error("Error", newData))
                    }
                }
            }

        }
    }

    fun asLiveData() = result as LiveData<BaseResponse<ResultType>>

    @WorkerThread
    protected abstract fun saveCallResult(item: RequestType)

    @WorkerThread
    protected open fun processResponse(response: RequestType) = response

    @MainThread
    protected abstract fun shouldFetch(data: ResultType?): Boolean

    @MainThread
    protected abstract fun loadFromDb(): LiveData<ResultType>

    @MainThread
    protected abstract fun createCall(): LiveData<RequestType>

    protected open fun onFetchFailed() {}

}
