package com.sample.faizan.currencyconversionapp.models


import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey

@Entity(tableName = "exchange_rate_entity", indices = [Index(value = ["id"], unique = true)])
data class ExchangeRateEntity(

    @PrimaryKey(autoGenerate = true)
    val id: Int? = null,
    val sourceCurrency: String = "",
    val currency: String = "",
    val amount: Double = 0.0,
    val lastSyncAt: Long = 0
)
