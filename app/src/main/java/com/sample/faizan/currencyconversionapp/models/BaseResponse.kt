package com.sample.faizan.currencyconversionapp.models

class BaseResponse<T>(
    var status: Status,
    var data: T? = null,
    var error: BaseError? = null,
    var message: String? = null,
    var code: Int? = null
) {

    companion object {

        fun <T> success(data: T, code: Int? = null): BaseResponse<T> {
            return BaseResponse(Status.SUCCESS, data, null, null, code)
        }

        fun <T> error(
            msg: String,
            data: T?,
            error: BaseError? = null,
            code: Int? = null
        ): BaseResponse<T> {
            return BaseResponse(Status.ERROR, data, error, msg, code)
        }

        fun <T> loading(data: T?): BaseResponse<T> {
            return BaseResponse(Status.LOADING, data, null)
        }

    }

}

data class BaseError(
    var message: String? = null,
    var developerMessage: String? = null,
    var status: Int? = null,
    var errorCode: Int? = null,
    var timeStamp: String? = null
)

enum class Status {
    LOADING,
    SUCCESS,
    ERROR
}