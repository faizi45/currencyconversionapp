package com.sample.faizan.currencyconversionapp.models

import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey


@Entity(tableName = "currency_entity", indices = [Index(value = ["id"], unique = true)])
data class CurrencyListEntity(
    @PrimaryKey(autoGenerate = true)
    val id: Int? = null,
    val currency: String = "",
    val currencyLabel: String = "",
    val lastSyncAt: Long = 0
)