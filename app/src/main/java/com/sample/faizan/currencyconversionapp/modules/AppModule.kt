package com.sample.faizan.currencyconversionapp.modules

import android.app.Application
import android.util.Log
import com.sample.faizan.currencyconversionapp.BuildConfig
import com.sample.faizan.currencyconversionapp.api.ApiService
import com.sample.faizan.currencyconversionapp.database.CurrenciesDatabase
import com.sample.faizan.currencyconversionapp.database.CurrenciesListDao
import com.sample.faizan.currencyconversionapp.utils.LiveDataCallAdapterFactory
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton


@Module
@InstallIn(SingletonComponent::class)
object AppModule {


    @Provides
    @Singleton
    fun provideInterceptor() =
        Interceptor { chain ->

            val url = chain.request()
                .url
                .newBuilder()
                .build()

            val request = chain.request()
                .newBuilder()
                .url(url)
                .build()

            Log.d("RAW_URL", url.toString())

            return@Interceptor chain.proceed(request)
        }

    @Provides
    @Singleton
    fun provideOkHttpClient(interceptor: Interceptor) =
        OkHttpClient
            .Builder()
            .addInterceptor(interceptor)
            .build()

    @Provides
    @Singleton
    fun provideRetrofitInstance(okHttpClient: OkHttpClient): ApiService =
        Retrofit.Builder()
            .client(okHttpClient)
            .baseUrl(BuildConfig.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(LiveDataCallAdapterFactory())
            .build()
            .create(ApiService::class.java)

    @Provides
    @Singleton
    fun providesCurrenciesDatabase(context: Application): CurrenciesDatabase {
        return CurrenciesDatabase.invoke(context)

    }

    @Provides
    @Singleton
    fun provideCurrenciesListDao(database: CurrenciesDatabase): CurrenciesListDao {
        return database.getCurrenciesListDao()
    }

}