package com.sample.faizan.currencyconversionapp.utils

import androidx.lifecycle.LiveData
import com.sample.faizan.currencyconversionapp.models.BaseResponse
import retrofit2.Call
import retrofit2.CallAdapter
import retrofit2.Callback
import retrofit2.Response
import java.lang.reflect.Type
import java.util.concurrent.atomic.AtomicBoolean

/**
 * A Retrofit adapter that converts the Call into a LiveData of ApiResponse.
 * @param <R>
</R> */

class LiveDataCallAdapter<T>(private val responseType: Type) :
    CallAdapter<T, LiveData<BaseResponse<T>>> {
    override fun responseType() = responseType
    override fun adapt(call: Call<T>): LiveData<BaseResponse<T>> {
        return object : LiveData<BaseResponse<T>>() {
            private var started = AtomicBoolean(false)
            override fun onActive() {
                super.onActive()
                if (started.compareAndSet(false, true)) {
                    call.enqueue(object : Callback<T> {
                        override fun onResponse(call: Call<T>, response: Response<T>) {
                            if (response.body() != null)
                                postValue(BaseResponse.success(response.body()!!))
                            else response?.body()?.let {
                                postValue(BaseResponse.error("Error", response.body()))

                            }
                        }

                        override fun onFailure(call: Call<T>, throwable: Throwable) {
                            postValue(BaseResponse.error(throwable.localizedMessage, null))
                        }
                    })
                }
            }
        }
    }
}