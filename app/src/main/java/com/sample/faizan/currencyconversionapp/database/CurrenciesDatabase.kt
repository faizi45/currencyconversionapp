package com.sample.faizan.currencyconversionapp.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.sample.faizan.currencyconversionapp.models.CurrencyListEntity
import com.sample.faizan.currencyconversionapp.models.ExchangeRateEntity

@Database(
    entities = [CurrencyListEntity::class, ExchangeRateEntity::class],
    version = 2,
    exportSchema = false
)

abstract class CurrenciesDatabase : RoomDatabase() {

    abstract fun getCurrenciesListDao(): CurrenciesListDao

    companion object {
        //bcz we want to have only one instance of database and not multiple instances
        //volatile means all threads will have same val

        @Volatile
        private var instance: CurrenciesDatabase? = null

        //to make sure no 2 threads are currently doing same thing
        private val LOCK = Any()

        operator fun invoke(context: Context) = instance ?: synchronized(LOCK) {
            instance ?: buildDatabase(context).also {
                instance = it
            }
        }

        private fun buildDatabase(context: Context) = Room.databaseBuilder(
            context.applicationContext,
            CurrenciesDatabase::class.java, "currencies.db"

        ).allowMainThreadQueries()
            .build()

    }
}