package com.sample.faizan.currencyconversionapp.ui

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.sample.faizan.currencyconversionapp.models.BaseResponse
import com.sample.faizan.currencyconversionapp.models.CurrencyListEntity
import com.sample.faizan.currencyconversionapp.models.ExchangeRateEntity
import com.sample.faizan.currencyconversionapp.repository.CurrencyRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class HomeViewModel @Inject constructor(private val repository: CurrencyRepository) : ViewModel() {

    var currenciesList: LiveData<BaseResponse<List<CurrencyListEntity>>>? = null

    var exchangeRatesList: LiveData<BaseResponse<List<ExchangeRateEntity>>>? = null

    init {

        fetchCurrenciesList()

        fetchExchangeRatesList()

    }

    private fun fetchCurrenciesList() {

        currenciesList = repository.getCurrenciesList()
    }

    private fun fetchExchangeRatesList() {

        exchangeRatesList = repository.getExchangeRates()
    }

}