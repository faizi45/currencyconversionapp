package com.sample.faizan.currencyconversionapp.api

import androidx.lifecycle.LiveData
import com.sample.faizan.currencyconversionapp.models.BaseResponse
import com.sample.faizan.currencyconversionapp.models.CurrenciesListResponseDto
import com.sample.faizan.currencyconversionapp.models.LiveConversionResponseDto
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {

    @GET("list")
    fun getCurrenciesList(
        @Query("access_key") accessKey: String
    ): LiveData<BaseResponse<CurrenciesListResponseDto>>

    @GET("live")
    fun getLiveExchangeRates(
        @Query("access_key") accessKey: String
    ): LiveData<BaseResponse<LiveConversionResponseDto>>

}